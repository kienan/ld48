extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	if GameState.difficulty == null:
		GameState.initialize(0)
	get_node("Ready").set_disabled(true)
	get_node("Ready").connect("pressed", self, "_on_ready_pressed")
	for c in get_node("Map").get_children():
		if c.get_name().match("Level?"):
			c.activate_options()
			c.connect("encounter_choice_changed", self, "_update_ready_state")

func _update_ready_state():
	var all_ready = true
	for c in get_node("Map").get_children():
		if c.get_name().match("Level?"):
			all_ready = all_ready and c.has_active()
	get_node("Ready").set_disabled(not all_ready)

func _on_ready_pressed():
	get_node("Ready").set_disabled(true)
	var picks = []
	for c in get_node("Map").get_children():
		if c.get_name().match("Level?"):
			picks.append(c.get_active_option_index())
	GameState.start_encounters(picks)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
