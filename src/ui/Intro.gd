extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var dialogue = {
	"init": [
		{
			"type": "set_speaker",
			"args": "res://assets/captain.png",
		},
		{
			"type": "reset_text",
		},
	],
	0.5: [
		{
			"type": "add_line",
			"args": [
				"Diligent's Captain",
				"[i]*bzzt*[/i] Mayday! Mayday! [i]*crackle*[/i]",
			]
		},
		{
			"type": "wait_for_space",
		}
	],
	1.0: [
		{
			"type": "add_line",
			"args": [
				"You",
				"This is the captain of the [b]Wellspring[/b]. What's your situation?",
			]
		},
		{
			"type": "add_line",
			"args": [
				"Diligent's Captain",
				"We've lost power and we're being dragged deeper into this gravity [i]*bzzt*[/i].\n" \
				+ "[i]*crack*[/i] warp crystal, if you can get here and take us out."
			],
		},
		{
			"type": "wait_for_space",
		}
	],
	1.5: [
		{
			"type": "add_line",
			"args": [
				"*thinking*",
				"Did I hear warp crystal?? We desperately need one if we're to return to Earth...\n\n",
			]	
		},
		{
			"type": "add_line",
			"args": [
				"You",
				"Acknowledged, we're on our way. Wellspring out.\n\n",
			]
		},
		{
			"type": "add_line",
			"args": [
				"Narrator",
				"[color=a20d0d]With your ship and crew, and the resources you have at and. You start plotting a course to rendezvous with the [b]Diligent[/b].[/color]"
			]
		},
		{
			"type": "wait_for_key",
			"args": [
				funcref(self, "end")
			]
		}
	],
}	
# Called when the node enters the scene tree for the first time.
func _ready():
	find_node("Dialogue").start(self.dialogue)
	

func _input(_ev):
	if Input.is_action_just_pressed("ui_cancel"):
		self.end()
		
func end():
	# Leave introduction
	get_node("Dialogue").end()
	GameState.end_introduction()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
