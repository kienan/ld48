extends PopupPanel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal encounter_complete

var data = null
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#if true:
	#	self.initialize(GameState.EncounterData["ArtificialSatellite"]["situations"][0])

func initialize(d):
	#for c in get_tree().get_nodes_in_group("ChoiceButtons"):
	#	c.deferred_call("free")
	self.data = d
	get_node("V/Title").set_bbcode("[center]" + data["title"] + "[/center]")
	get_node("V/Description").set_bbcode(data["text"])
	if data["picture"]:
		get_node("V/Picture").set_texture(ResourceLoader.load(data['picture']))
	for i in data['choices'].keys():
		var b = Button.new()
		b.set_text(data['choices'][i]['title'])
		var tt = data['choices'][i]['tooltip']
		if GameState.debug:
			tt += "\n"
			var index = 0
			for r in data['choices'][i]['results'].keys():
				var chance = 0
				if index + 1 < len(data['choices'][i]['results'].keys()):
					chance = data['choices'][i]['results'].keys()[index+1] - r
				else:
					chance = 100 - r
				tt += str(chance) + "% chance of " + data['choices'][i]['results'][r]['title'] + "\n"
				index += 1
		b.set_tooltip(tt)
		b.add_to_group("ChoiceButtons")
		get_node("V/Menu").add_child(b)
		b.connect("pressed", self, "_on_choice_made", [i])

func _on_choice_made(index):
	for c in get_tree().get_nodes_in_group("ChoiceButtons"):
		c.set_disabled(true)
	var prev = 0
	var result_indices = self.data["choices"][index]["results"].keys()
	var result = null
	var chance = GameState.rng.randi_range(0, 100)
	#print(chance)
	var i = len(result_indices) - 1 
	while i >= 0:
		if chance >= result_indices[i]:
			result = result_indices[i]
			break
		else:
			i -= 1
	#print(result)
	#print(self.data["choices"][index]["results"][result])
	var action = self.data["choices"][index]["results"][result]
	if action.has("stat_changes"):
		GameState.update_stats(action["stat_changes"])
	if action.has("orbit_changes"):
		GameState.update_orbit(action["orbit_changes"])
	if action.has("crew_changes"):
		GameState.update_crew(action["crew_changes"])
	#print(action)
	var t = RichTextLabel.new()
	t.set_use_bbcode(true)
	var text = self.data["choices"][index]["then"]
	if action.has("text"):
		text += "\n\n" + action['text']
		if action.has("stat_changes") and action["stat_changes"]:
			text += "\n\n[center]Effects:[/center]\n"
			for k in action["stat_changes"].keys():
				#print(k)
				var value = action["stat_changes"][k]
				var color = "green"
				if value < 0:
					color = "red"
				#print(color)
				var addition = k + ": [color=" + color + "]" + str(value) + "[/color]\n"
				#print(addition)
				text += addition
	t.set_bbcode(text)
	t.set_fit_content_height(true)
	get_node("V").add_child(t)
	var exit = Button.new()
	exit.set_text("Continue")
	get_node("V").add_child(exit)
	exit.connect("pressed", self, "_on_exit_pressed")

func _on_exit_pressed():
	emit_signal("encounter_complete")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
