extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
const crew_per_page = 4
var current_page = 0
var entries = {}

func _ready():
	self.current_page = 0
	GameState.connect("crew_updated", self, "_on_crew_update")
	self.draw_crew()

func _on_crew_update():
	for c in get_tree().get_nodes_in_group("CrewEntries"):
		c.set_visible(false)
		c.call_deferred("free")
	self.draw_crew()

func draw_crew():
	self.entries = {}
	var count = len(GameState.ship_stats['crew'])
	var max_pages = (count / self.crew_per_page) + 1
	self.print_tree_pretty()
	if self.current_page == 0:
		get_node("Container/Pager/Previous").set_disabled(true)
	get_node("Container/Display").set_text(str(self.current_page + 1 ) + " / " + str(max_pages))
	print(max_pages)
	if max_pages < 2 or ((current_page+1) >= max_pages):
		get_node("Container/Pager/Previous").set_disabled(true)
	var crew_entry_ui = ResourceLoader.load("res://src/ui/CrewEntry.tscn")
	for c in GameState.ship_stats['crew']:
		var e = crew_entry_ui.instance();
		e.initialize(c)
		entries[c['id']] = e
		e.add_to_group("CrewEntries")
		e.set_custom_minimum_size(Vector2(100, 137))
	var x = 0 + (current_page * crew_per_page);
	var limit = x + crew_per_page
	while x < limit:
		if x >= len(entries.keys()):
			break;
		var key = entries.keys()[x]
		get_node("Container/Crew").add_child(entries[key])
		x += 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
