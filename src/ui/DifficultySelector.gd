extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var buttonNames = [
	"EasyButton",
	"MediumButton",
	"HardButton"
]
# Called when the node enters the scene tree for the first time.
func _ready():
	# Bind button callbacks
	for n in buttonNames:
		var button = self.find_node(n)
		button.connect("toggled", self, "_on_difficulty_button_pressed", [n])
	self.find_node("MediumButton").set_pressed(true)
	self.find_node("StartButton").connect("pressed", self, "_on_start_button_pressed")

func _on_difficulty_button_pressed(state, buttonName):
	var any_active = state
	for n in buttonNames:
		if n != buttonName:
			if state:
				self.find_node(n).set_pressed(false)
			any_active = any_active or self.find_node(n).is_pressed()
	self.find_node("StartButton").disabled = not any_active

func _on_start_button_pressed():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
