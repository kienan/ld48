extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal encounter_choice_changed

var choices = 3
var level = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	# Bind button callbacks
	#for n in buttonNames:
	#	var button = self.find_node(n)
	#	if button:
	#		button.connect("toggled", self, "_on_option_button_pressed", [n])
	pass

func initialize(l = null, nOptions = null):
	self.level = l
	print("Intializing choices for level " + str(l) + " with " + str(nOptions) + " options")
	if nOptions == null:
		nOptions = 3
	self.choices = nOptions
	var x = 0
	while x < nOptions:
		var s = get_node("Spot" + str(x))
		var n = TextureButton.new()
		if s:
			n.set_position(s.get_position())
		n.set_name("Option" + str(x))
		n.add_to_group("EncounterOptions")
		n.set_toggle_mode(true)
		#print("Adding option with name: " + n.get_name())
		n.connect("toggled", self, "_on_option_button_pressed", [n.get_name()])
		n.set_normal_texture(ResourceLoader.load("res://assets/encounterPlaceholder0.png"))
		n.set_pressed_texture(ResourceLoader.load("res://assets/encounterIconPlaceholder0_pressed.png"))
		add_child(n, true)
		print("Adding texture button: Option" + str(x))
		#self.find_node("Option" + str(x)).visible = x < nOptions
		x += 1

func activate_options():
	for c in get_my_options():
		if c.visible:
			c.set_disabled(false)

func active_visited():
	var i = self.get_active_option_index()
	if i == null:
		print(self.get_name() + " has no active option index")
		return false
	if self.level == null:
		print(self.get_name() + " does not have self.level set")
		return false
	print("Level " + str(self.level) + " is visited: " + str(GameState.encounters[self.level][i]['visited']))
	return GameState.encounters[self.level][i]['visited']

func disable_options():
	for c in get_my_options():
		if c.visible:
			c.set_disabled(true)

func get_active_option_index():
	var x = 0
	print("Checking for active index of level " + str(level))
	for c in get_my_options():
		var i = int(c.get_name()[-1])
		if GameState.encounters[self.level][i]['visited']:
			print(c.get_name() + " is visited, skipping")
			x += 1
			continue
		if not c.is_visible():
			print(c.get_name() + " is not visible, skipping")
			x += 1
			continue
		if c.is_pressed() or GameState.encounters[self.level][i]['selected']:
			print(c.get_name() + " is pressed")
			return x
		else:
			print(c.get_name() + " is not pressed, skipping")
			x += 1
	return null
	
func get_active_position():
	var i = get_active_option_index()
	if i == null:
		return null
	var n = "Option" + str(i)
	if get_node(n):
		return get_node(n).get_global_position()
	else:
		return null

func has_active():
	var active = false
	for n in get_my_options():
			active = active or (n.is_pressed() and n.is_visible())
	return active

func get_my_options():
	var n = []
	if not get_tree():
		return n
	for o in get_tree().get_nodes_in_group("EncounterOptions"):
		if o.get_parent() != self:
			continue
		n.append(o)
	return n

func _on_option_button_pressed(state, buttonName):
	# Map calls this before we have buttons. Or before we exist?
	#print(buttonName + " has state: " + str(state))
	for n in get_my_options():
		#print(n.get_name())
		if n.get_name() != buttonName:
			if state :
				n.set_pressed(false)
	emit_signal("encounter_choice_changed")
	#for n in buttonNames:
	#	if self.find_node(n).is_pressed():
	#		print("Button" + n + " is pressed")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
