extends PopupMenu


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(_ev):
	if Input.is_action_just_pressed("ui_cancel"):
		var menu = self
		if menu.is_visible():
			menu.set_visible(false)
		else:
			menu.popup_centered();

func _on_ContinueButton_pressed():
	self.set_visible(false)


func _on_ExitGameButton_pressed():
	get_tree().quit()


func _on_LeaveGameButton_pressed():
	GameState.reset_to_main_menu()
