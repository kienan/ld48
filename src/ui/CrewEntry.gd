extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func initialize(data):
	find_node("Name").set_bbcode("[center]" + data['name'] + "[/center]")
	if data['morale']:
		find_node("Morale").set_value(data['morale'])
	else:
		get_node("Container/Morale").set_visible(false)
	if data['picture']:
		get_node("Container/Image").set_texture(ResourceLoader.load(data['picture']))
	else:
		get_node("Container/Image").set_texture(ResourceLoader.load("res://assets/crewPlaceholder.png"))
	var tt = ""
	if data['tooltip']:
		tt = data['tooltip']
	tt += "\n\n"
	for i in data.keys():
		if i == "tooltip":
			continue
		tt += i + ": " + str(data[i]) + "\n\n"
	find_node("Image").set_tooltip(tt)
