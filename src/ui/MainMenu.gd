extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	# Connect start button with our callback
	self.find_node("DifficultySelector").find_node("StartButton").connect("pressed", self, "_on_start_button_pressed")
	GameState.current_scene = self

func _on_start_button_pressed():
	# Get the difficulty button that is pressed
	var chosen_difficulty = 0
	#var state = get_tree().get_root().find_node("GameState")
	for d in GameState.difficulties.keys():
		if self.find_node(d+ "Button").is_pressed():
			chosen_difficulty = GameState.difficulties[d]
			break
	print("Chosen difficulty: " + GameState.difficulties.keys()[chosen_difficulty] + "(" + str(chosen_difficulty) + ")")
	self.find_node("DifficultySelector").find_node("StartButton").disconnect("pressed", self, "_on_start_button_pressed")
	GameState.initialize(chosen_difficulty)
	GameState.start_introduction()

func _input(_ev):
	if Input.is_action_just_pressed("ui_cancel"):
		self._on_Exit_pressed()

func _on_Exit_pressed():
	get_tree().quit()
