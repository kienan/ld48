extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var going_deeper = null
var direction = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	# This is for debugging when choosing the scene directly.
	if GameState.current_state == 0:
		GameState.current_state = 5
		GameState.current_scene = self
	# End
	
	if GameState.current_state == 8:
		direction = -1
	var encounter = get_node("Map/Level" + str(GameState.current_depth))
	if encounter:
		GameState.ship_stats["target_position"] = encounter.get_active_position()
	get_node("Map").connect("ship_arrived", self, "_on_ship_arrived")

func _on_ship_arrived():
	#var encounter = get_node("Map/Level" + str(GameState.current_depth))
	#if not encounter:
	#	return
	#
	if GameState.current_state != 5 and GameState.current_state != 8:
		return
	# Work around a duplicate event bug
	if GameState.current_state == 8 and self.direction == 1:
		return
	var index = get_node("Map/Level" + str(GameState.current_depth)).get_active_option_index()
	if index == null:
		var n = get_node("Map/Level" + str(GameState.current_depth))
		print(GameState.encounters[GameState.current_depth])
	print("Got index for level " + str(GameState.current_depth) + ": " + str(index))
	GameState.encounters[GameState.current_depth][index]['visited'] = true
	get_node("Map").draw_course()
	var e = GameState.encounters[GameState.current_depth]
	var instance = GameState.generate_encounter_instance(e, GameState.current_depth, index)
	instance.add_to_group("Encounters")
	self.add_child(instance)
	instance.connect("encounter_complete", self, "_on_encounter_complete", [instance])
	instance.popup_centered()

func _on_encounter_complete(instance):
	instance.set_visible(false)
	self.next_encounter()

func next_encounter():
	print("Heading to next encounter")
	print(GameState.current_depth)
	print(GameState.current_state)
	print(GameState.encounters)
	if GameState.current_state != 5 and GameState.current_state != 8:
		return
	if (self.direction == 1 and GameState.current_depth < (len(GameState.encounters) - 1)) \
	 or (self.direction == -1 and GameState.current_depth > 0):
		GameState.current_depth += self.direction
		GameState.update_stats({
			"deltav": -GameState.orbit_stats["drag"],
			"supplies": -GameState.get_supply_upkeep(),
		})
		if get_node("Map/Level" + str(GameState.current_depth)):
			if get_node("Map/Level" + str(GameState.current_depth)).get_active_position():
				GameState.update_stats({
					"target_position": get_node("Map/Level" + str(GameState.current_depth)).get_active_position(),
				})
		GameState.update_orbit({
			"altitude": -self.direction * (GameState.get_difficulty_data("starting_altitude") - GameState.get_difficulty_data("minimum_altitude")) / len(GameState.encounters)
		})
	else:
		# Provide a choice: go deeper more reward, or start the journey out.
		# Ignore going_deeper for the moment, just try to get out
		if GameState.current_state == 5:
			print("Going for flight out")
			GameState.choose_flightpath_out()
		elif GameState.current_state == 8:
			GameState.end(true, "You made it out")
		# These will be re-ordered
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
