extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var timer = 0
var waiting = true
var wait_condition = null
var data = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func start(d):
	self.data = d
	self.timer = 0
	self.process_step("init")
	self.waiting = false

func end():
	self.waiting = true

func _input(ev):
	if self.waiting:
		if self.wait_condition is String and self.wait_condition == "space" and Input.is_key_pressed(KEY_SPACE):
			self.waiting = false
			self.wait_condition = null
		if self.wait_condition is Array and ev is InputEventKey and ev.pressed:
			self.waiting = false
			self.wait_condition[0].call_func()
			self.wait_condition = null

		
func process_step(id):
	print(self.data.keys())
	if not self.data.keys().has(id):
		print("Step '" + id + "' not found. Skipping")
		return
	for action in self.data[id]:
		print("Running action: " + action["type"])
		if action["type"] == "set_speaker":
			self.find_node("Speaker").set_texture(ResourceLoader.load(action["args"]))
		elif action["type"] == "reset_text":
			self.find_node("Text").set_bbcode("")
		elif action["type"] == "add_line":
			self.find_node("Text").append_bbcode("[b]" + action["args"][0] + ": [/b]")
			self.find_node("Text").append_bbcode(action["args"][1] + "\n")
		elif action["type"] == "wait_for_space":
			self.find_node("Text").append_bbcode("[center][i][pulse color=#00FFAA height=0.0 freq=4.0]Hit space to continue[/pulse][/i][/center]\n")
			self.waiting = true
			self.wait_condition = "space"
		elif action["type"] == "wait_for_key":
			self.find_node("Text").append_bbcode("[center][i][pulse color=#00FFAA height=0.0 freq=4.0]Press any key to continue[/pulse][/i][/center]\n")
			self.waiting = true
			self.wait_condition = action["args"]
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not waiting:
		var old_timer = self.timer
		self.timer += delta
		for k in self.data.keys():
			if str(k) != "init" and k > old_timer and k <= self.timer:
				self.process_step(k)
