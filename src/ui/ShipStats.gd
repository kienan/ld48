extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.connect("stats_updated", self, "_on_update_stats")
	GameState.connect("orbit_updated", self, "_on_update_stats")
	self.update_stats()
	
func _on_update_stats():
	self.update_stats()
	
func update_stats():
	get_node("Container/Health").set_text(
		"Health: " + str(GameState.ship_stats["health"]) + " / 1000"
	)
	get_node("Container/Supplies").set_text(
		"Supplies: " + str(GameState.ship_stats["supplies"]) + " / 1000 [upkeep: " + str(GameState.get_supply_upkeep()) +"]"
	)
	get_node("Container/Crew").set_bbcode(
		"Crew: " + str(len(GameState.ship_stats["crew"])) + " / " \
		+ str(GameState.get_difficulty_data("maximum_crew"))
	)
	get_node("Container/DeltaV").set_bbcode("DeltaV: " + str(GameState.ship_stats["deltav"]))
	get_node("Container/Altitude").set_text(
		"Altitude: " + str(GameState.orbit_stats["altitude"]) + " [minimum: " \
		+ str(GameState.get_difficulty_data("minimum_altitude")) + "]"
	)
	get_node("Container/Drag").set_text(
		"Drag: " + str(GameState.orbit_stats["drag"])
	)
	get_node("Container/LowestOrbit").set_text(
		"Lowest orbit: " + str(GameState.orbit_stats["record_low"])
	)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
