extends PopupPanel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func initialize(state, reason):
	var t = ""
	if state:
		t = "[center][rainbow freq=0.5 sat=2 val=200]You won![/rainbow][/center]\n\n"

	else:
		t = "[center][color=red]You lost![/color][/center]\n\n"
	t += "[center]Because: " + reason + "[/center]\n\n"
	t += "[center]Low Score: " + str(GameState.orbit_stats['record_low']) + " (lower is better!) [/center]"
	get_node("VBoxContainer/Label").set_bbcode(t)
	

func _on_New_pressed():
	self.call_deferred("free")
	GameState.reset_to_new()


func _on_Main_pressed():
	self.call_deferred("free")
	GameState.reset_to_main_menu()


func _on_Exit_pressed():
	get_tree().quit()
