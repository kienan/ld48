extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ship_arrived

var ship_moving = false

# Called when the node enters the scene tree for the first time.
func _ready():
	# For testing
	var started_ourselves = false
	if GameState.difficulty == null:
		GameState.initialize(0)
		started_ourselves = true
		
	#
	self.get_node("Ship").set_global_position(GameState.ship_stats['starting_position'])
	self.get_node("Planet").set_texture(ResourceLoader.load(GameState.get_difficulty_data("planet")))
	var encounterChoice = ResourceLoader.load("res://src/ui/EncounterChoice.tscn")
	for level in GameState.encounters.keys():
		#print(level)
		var name = "Level" + str(level)
		var choice = encounterChoice.instance()
		choice.set_name(name)
		choice.initialize(level, len(GameState.encounters[level]))
		choice.set_global_position(get_node("Level" + str(level) + "Location").get_global_position())
		choice.set_global_rotation(get_node("Level" + str(level) + "Location").get_global_rotation())
		choice.set_global_scale(get_node("Level" + str(level) + "Location").get_global_scale())
		var x = 0
		while x < len(GameState.encounters[level]):
			# Fixes the rotation of the individual buttons
			var n  = choice.get_node("Option" + str(x))
			if not n:
				x += 1
				continue
			n.set_rotation(
				-(find_node("Level" + str(level) + "Location").get_global_rotation())
				)
			# Set icon and tooltip
			#print("Checking for icons related to '" + GameState.encounters[level][x]["type"] + "'")
			var texture = ResourceLoader.load("res://assets/encounterIcon" + GameState.encounters[level][x]["type"] + ".png")
			var texture_pressed = ResourceLoader.load("res://assets/encounterIcon" + GameState.encounters[level][x]["type"] + "_pressed.png")
			if texture:
				n.set_normal_texture(texture)
			if texture_pressed:
				n.set_pressed_texture(texture_pressed)
			#n.set_disabled(true)
			n.set_tooltip(GameState.encounters[level][x]["type"])
			if GameState.encounters[level][x]["selected"] or (x == 0 and started_ourselves):
				# The texture change is used since when disabled the normal_texture is shown regardless of press state.
				n.set_pressed(true)
				#choice._on_option_button_pressed(true, "Option" + str(x))
				#n.set_normal_texture(n.get_pressed_texture())
			if GameState.encounters[level][x]["visited"]:
				n.set_visible(false)
				# Don't free, because we have some bugs where we refer to the option again.
				# This is a problem and leads to weird behaviour but I guess we'll have to live
				# with it.
				#n.free()
			x += 1
		if GameState.current_state == 4: # picking course
			choice.activate_options()
		self.add_child(choice)
		choice.add_to_group("Levels")
		choice.connect("encounter_choice_changed", self, "_on_encounter_choice_changed")
	self.draw_course()

func _process(_delta):
	var s = get_node("Ship")
	if GameState.ship_stats["target_position"] == null:
		GameState.ship_stats["target_position"] = Vector2(0, 0)
	if s.get_global_position().distance_to(GameState.ship_stats["target_position"]) >= 5:
		ship_moving = true
		s.set_global_position(s.get_global_position().move_toward(GameState.ship_stats["target_position"], 5.0))
	else:
		if ship_moving:
			emit_signal("ship_arrived")
			draw_course()
		ship_moving = false
		

func _on_encounter_choice_changed():
	draw_course()
	
func draw_course():
	# A bit brutal
	for p in get_tree().get_nodes_in_group("Course"):
		#print(p.get_name())
		p.set_visible(false)
		p.call_deferred("free")

	var x = 0
	var pairs = []
	while x < (len(GameState.encounters) - 1):
		pairs.append([x, x+1])
		x += 1
	#print(pairs)
	for p in pairs:
		var first = get_node("Level" + str(p[0]))
		var second = get_node("Level" + str(p[1]))
		if not first or not second:
			#print("One is null: " + str(p))
			continue
		#if not first.has_active() or not second.has_active():
		#	print("One has no active: " + str(p))
		#	continue
		#if second.active_visited():
		#	print("Second has a visited active choice: " + str(p))
		#	continue
		if not first.get_active_position() or not second.get_active_position():
			#print("One has no posiion: " + str(p))
			continue
		var l = Line2D.new()
		l.add_point(first.get_active_position())
		l.add_point(second.get_active_position())
		l.set_z_index(-1)
		self.add_child(l)
		l.add_to_group("Course")
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
